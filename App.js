import React, { Component } from 'react';
import {
  Button,
  StyleSheet,
  Text,
  View
} from 'react-native';
import SocketIOClient from 'socket.io-client';

export default class App extends Component<{}> {
  state = {
    title: 'Texto inicial',
    backgroundColor: 'white',
    fontColor: 'black'
  }

  constructor(props) {
      super(props);

      // Creating the socket-client instance will automatically connect to the server.
      this.socket = SocketIOClient('http://192.168.0.254:3000');
      this.socket.on('changed background', this._onChangedBackground);
  }

  _onChangedBackground = (newBackground) => {
    this.setState({
      title: this.state.title,
      backgroundColor: newBackground,
      fontColor: 'white'
    });
  }

  _onPressedButton = () => {
    this.socket.emit('new message', 'msg');
  }

  render() {
    return (
      <View
        backgroundColor= {this.state.backgroundColor}
        style={styles.container}>

        <Text style={styles.welcome} color={this.state.fontColor}>
          { this.state.backgroundColor }
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});
