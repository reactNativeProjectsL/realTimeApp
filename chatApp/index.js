var express = require('express');
var http = require('http')
var socketio = require('socket.io');
var path = require('path');

const PORT = 3000;

var app = express();
var server = http.Server(app);
var io = socketio(server, {pingTimeout: 30000});

io.on('connection', (socket) => {
  console.log('A client just joined on', socket.id);

  socket.on('change background', (newBackground) => {
    socket.broadcast.emit('changed background', newBackground);
  });
});

app.get('/', (req, res) => {
  res.sendFile(path.resolve('templates/index.html'));
});

server.listen(PORT, () => {
  console.log(`Up in port ${PORT}`);
});
